# Grupo 25

## Corrección

    Tag o commit corregido:	lab-3
    		Pesos internos

### Entrega 100.00% 1

    Tag correcto	100.00%	0.5
    En tiempo	100.00%	0.5

### Funcionalidad 100.00% 1

    "Current" funciona correctamente (se actualiza cada vez que se busca una nueva ciudad)	100.00%	0.3
    "Forecast" funciona correctamente (se muestra el listado de dias y si hago click veo el detalle para un dia)	100.00%	0.4
    El search box funciona correctament (si ingreso el nombre de una ciudad y presiono el boton se actualiza la información)	100.00%	0.2
    Se muestra un spinning wheel throbber cuando se buscan los datos de una ciudad	100.00%	0.05
    Los errores (si los hay) se muestran con un mensaje en pantalla. 	100.00%	0.05

### Modularización y diseño 100.00% 1

    Los componentes manejan su propio estado y/o el de sus componentes hijos. No se comparte estado entre componentes sibling.	100.00%	0.3
    Todo componente sin estado es un componente funcional (e.g. no se implementa como una clase)	100.00%	0.25
    Componentes distintos en archivos distintos. No se requiere específicamente un componente por archivo, pero sí que la modularización sea efectiva. Es decir, los componentes que son claramente distintos y tienen propósitos distintos no deberían estar en el mismo archivo. 	100.00%	0.1
    No se repite codigo innecesariamente, correcta elección de abstracciones	100.00%	0.1
    Todos los componentes hacen chequeo de tipos utilizando PropTypes.	100.00%	0.25

### Calidad de código 90.00% 1

    El README indica claramente como instalar el proyecto y setupear el linter	80.00%	0.5
    El linter Eslint no reporta errores de estilo	100.00%	0.5

### Uso de git 90.00% 1

    Commits frecuentes	100.00%	0.3
    Nombres de commits significativos	100.00%	0.2
    Commits de todes les integrantes	80.00%	0.5

### Opcionales 70.00% 1

    Punto estrella: Seccion UVI	100.00%	0.7
    Punto estrella: Utilización de la GeolocalizationAPI del browser para auto completar el search box	0.00%	0.2
    Punto Estrella: Permitir que el usuario pueda elegir el sistema métrico en el que quiere expresada la información	0.00%	0.1


# Nota Final 10

# Comentarios

    - Medio pobre el README
