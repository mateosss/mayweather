# May Weather

***Correr con `npm install && npm start`***

# Librerías

- **Lodash**: Una libería muy útil y eficiente con utilidades comunes que no
  están en el standard de ecmascript.

- **Material UI**: Framework ui de material design, a pesar de que hicimos el css en
  gran parte por nuestra cuenta, es bueno tener un set de componentes
  preimplementadas de ui para mockupear rápido y por si no se llega con el
  tiempo.
- **JSS**: Algo interesante que nos ofreción Material UI fue la integración
  de jss, una librería para escribir css en jsx, lo cual puede estar bueno
  para tener lógica, contenido y estilo en el mismo jsx.

# Decisiones de Diseño

Pudimos mantener el estado casi únicamente en la `App`, (más un estado pequeño
en `Forecast` para seguimiento del día actual), haciendo que todas las demás
componentes sean, en efecto, puramente declarativas.

El flujo del programa es tal que `App` se encarga de hacer el trabajo sucio
(manejar el input, hacer las api calls asíncronas, y actualizar su estado), y
luego esta usa el estado para re-renderizar las componentes declarativas que le
siguen en el DOM.

# Decisiones de UI

Quisimos hacer algo propio con la estética, ya que material design v1 está un
poco pasado de moda, así que nos separamos un poco del framework, e hicimos
nuestros propios diseños con css (*jss*). La idea fue hacer una card principal
(`DayCard`) que luego marcaría el resto del diseño minimalista de la página.

Funciona muy mal a nivel performance, y la UX no está demasiado pulida, pero
creemos que igualmente se llegó a un resultado interesante.
