import React from "react";
import PropTypes from "prop-types";

import { InputBase, IconButton, Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import SearchIcon from "@material-ui/icons/Search";
import { GooSpinner } from "react-spinners-kit";

const useStyles = makeStyles(() => ({
  root: {
    borderRadius: "0.5rem 2rem",
    height: "6rem",
    margin: "4rem auto",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    transition:
      "width 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, border-radius 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
    width: "75%",
    "&:focus-within": {
      borderRadius: "2rem 0.5rem",
      width: "100%"
    }
  },
  search: {
    width: "100%",
    display: "flex",
    padding: "0 4rem"
  },
  inputRoot: {
    flexGrow: 1
  },
  inputInput: {
    fontSize: "1.5rem",
    "&:focus": {
      // fontWeight: "bold",
    }
  },
  inputIcon: {
    width: "2.5rem",
    height: "2.5rem",
    opacity: 0.25
  }
}));

function SearchBox(props) {
  const classes = useStyles();
  const { onChange, onSubmit, loading } = props;
  return (
    <Paper className={classes.root}>
      <div className={classes.search}>
        <InputBase
          placeholder="Search…"
          onChange={onChange}
          classes={{
            root: classes.inputRoot,
            input: classes.inputInput
          }}
          onKeyDown={e => {
            if (e.keyCode === 13) onSubmit();
          }}
        />
        <IconButton color="inherit" onClick={onSubmit}>
          {!loading && <SearchIcon className={classes.inputIcon} />}
          <GooSpinner
            loading={loading}
            size={42}
            style={{ margin: 0, padding: 0, opacity: 0.5 }}
            color="#E91E63"
          />
        </IconButton>
      </div>
    </Paper>
  );
}

SearchBox.propTypes = {
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired
};

export default SearchBox;
