import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";

import { Grid, CardContent } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import ForecastDayCard from "./ForecastDayCard";
import DayCard from "./DayCard";

const isToday = date => {
  let today = new Date();
  const dd = String(today.getDate()).padStart(2, "0");
  const mm = String(today.getMonth() + 1).padStart(2, "0"); // January is 0!
  const yyyy = today.getFullYear();
  today = `${yyyy}-${mm}-${dd}`;

  return today === date;
};

const parseData = list => _.groupBy(list, elem => elem.dt_txt.split(" ", 1)[0]);

const useStyles = makeStyles(() => ({
  root: {
    padding: "4rem 0 !important"
  },
  grid: {
    width: "calc(100% + 12rem)",
    marginLeft: "-6rem"
  }
}));

function Forecast(props) {
  const { list } = props;
  if (_.isEmpty(props)) return <i />;

  const forecastData = parseData(list);
  const classes = useStyles();
  const [forecastDay, setForecastDay] = React.useState(null);

  function handleClick(newValue) {
    setForecastDay(newValue);
  }

  return (
    <div>
      <CardContent className={classes.root}>
        <Grid container justify="center" spacing={2} className={classes.grid}>
          {_.map(forecastData, (day, date) => {
            if (isToday(date)) return null;
            const p = {
              temp_min: day.reduce(
                (min, q) => (q.main.temp_min < min ? q.main.temp_min : min),
                day[0].main.temp_min
              ),
              temp_max: day.reduce(
                (max, q) => (q.main.temp_max > max ? q.main.temp_max : max),
                day[0].main.temp_max
              ),
              dt: day[0].dt,
              id: day[0].weather[0].id
            };

            return (
              <Grid item xs key={date} onClick={() => handleClick(date)}>
                <ForecastDayCard {...p} />
              </Grid>
            );
          })}
        </Grid>
      </CardContent>
      <Grid container direction="column" spacing={2}>
        {forecastDay &&
          forecastData[forecastDay].map(info => (
            <Grid key={info.dt} item>
              <DayCard weather={info} />
            </Grid>
          ))}
      </Grid>
    </div>
  );
}

Forecast.propTypes = {
  list: PropTypes.arrayOf(
    PropTypes.shape({
      dt_txt: PropTypes.string.isRequired,
      main: PropTypes.shape({
        temp_min: PropTypes.number.isRequired,
        temp_max: PropTypes.number.isRequired
      }).isRequired,
      dt: PropTypes.number.isRequired,
      weather: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.number.isRequired
        })
      )
    }).isRequired
  ).isRequired
};

export default Forecast;
