import React from "react";
import PropTypes from "prop-types";

import { Typography, Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { AccessTime } from "@material-ui/icons";

import { WeatherIcon, weatherBackgroundFor } from "./WeatherIcon";

const useStyles = makeStyles(theme => ({
  root: {
    position: "relative",
    padding: theme.spacing(3, 2),
    backgroundSize: "cover",
    backgroundPosition: "bottom",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    backgroundBlendMode: "color",
    maxWidth: "50rem",
    height: "20rem",
    margin: "0 auto",
    color: "white",
    borderRadius: "1rem 6rem",
    display: "flex"
  },
  mainIcon: {
    width: "25%",
    display: "flex",
    padding: "1rem",
    flexFlow: "column wrap",
    alignContent: "center",
    "& div": {
      fontSize: "6rem",
      flexGrow: 50,
      alignSelf: "center"
    },
    "& h4": {
      flexGrow: 20,
      textAlign: "center",
      lineHeight: "1rem",
      fontWeight: "bold"
    },
    "& h6": {
      flexGrow: 5,
      textAlign: "center",
      fontSize: "1rem",
      fontWeight: "normal"
    }
  },
  detailsContainer: {
    display: "flex",
    flexFlow: "column wrap",
    justifyContent: "center",
    alignContent: "space-evenly",
    width: "75%",
    height: "100%",
    position: "absolute",
    bottom: "0",
    right: "0",
    padding: "1rem 1rem 1rem 0"
  },
  detailItem: {
    padding: "1rem",
    display: "flex",
    "& div": {
      width: "3rem",
      height: "3rem",
      textAlign: "center",
      lineHeight: "4rem"
    },
    "& i": {
      fontSize: "2rem"
    },
    "& p": {
      paddingLeft: "1rem"
    }
  },
  itemBold: {
    fontWeight: "bold"
  }
}));

function Item(props) {
  const { icon, label, text } = props;
  const classes = useStyles();
  return (
    <div className={classes.detailItem}>
      <WeatherIcon icon={icon} />
      <Typography>
        {label}
        <br />
        <span className={classes.itemBold}>{text}</span>
      </Typography>
    </div>
  );
}

Item.propTypes = {
  icon: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired
};

function DayCard(props) {
  const { weather, style } = props;

  if (!weather) return <i />;
  const classes = useStyles();

  // If needed somewhere else, make a utility file
  const formatAMPM = epoch => {
    const date = new Date(epoch);
    let hours = date.getHours();
    let minutes = date.getMinutes();
    const suffix = hours >= 12 ? "pm" : "am";
    hours %= 12;
    if (!hours) hours = 12;
    minutes = minutes < 10 ? `0${minutes}` : minutes;
    return `${hours}:${minutes} ${suffix}`;
  };

  return (
    <div style={style}>
      <Paper
        className={classes.root}
        style={{
          backgroundImage: `url("/img/cards/${weatherBackgroundFor(
            weather.weather[0].id
          )}.jpg")`
        }}
      >
        <div className={classes.mainIcon}>
          <WeatherIcon icon={weather.weather[0].id} />
          <Typography variant="h4">
            {`${Math.round(weather.main.temp)}°C`}
          </Typography>
          <Typography variant="h6">{weather.weather[0].main}</Typography>
        </div>

        <div className={classes.detailsContainer}>
          {/* TODO: This is a copy of an Item but with another icon */}
          <div className={classes.detailItem}>
            <AccessTime style={{ width: 48, height: 48 }} />
            <Typography>
              Hour
              <br />
              <span className={classes.itemBold}>
                {formatAMPM(weather.dt * 1000)}
              </span>
            </Typography>
          </div>

          <Item
            icon="thermometer-exterior"
            label="Min Temp"
            text={`${Math.round(weather.main.temp_min)}˚C`}
          />
          <Item
            icon="thermometer"
            label="Max Temp"
            text={`${Math.round(weather.main.temp_max)}˚C`}
          />
          <Item
            icon="humidity"
            label="Humidty"
            text={`${weather.main.humidity}%`}
          />
          <Item
            icon="barometer"
            label="Preassure"
            text={`${Math.round(weather.main.pressure)} hpm`}
          />
          <Item
            icon="strong-wind"
            label="Wind"
            text={`${weather.wind.speed}km/h`}
          />
          {weather.sys.sunrise && (
            <Item
              icon="sunrise"
              label="Sunrise"
              text={formatAMPM(weather.sys.sunrise * 1000)}
            />
          )}
          {weather.sys.sunset && (
            <Item
              icon="sunset"
              label="Sunset"
              text={formatAMPM(weather.sys.sunset * 1000)}
            />
          )}
        </div>
      </Paper>
    </div>
  );
}

DayCard.propTypes = {
  weather: PropTypes.shape({
    dt: PropTypes.number.isRequired,
    main: PropTypes.shape({
      temp: PropTypes.number.isRequired,
      pressure: PropTypes.number.isRequired,
      humidity: PropTypes.number.isRequired,
      temp_min: PropTypes.number.isRequired,
      temp_max: PropTypes.number.isRequired
    }).isRequired,

    sys: PropTypes.object.isRequired,
    weather: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        main: PropTypes.string.isRequired
      })
    ).isRequired
  }).isRequired,
  style: PropTypes.object // style is required for Grow MUI component
};

DayCard.defaultProps = {
  style: {}
};

export default DayCard;
