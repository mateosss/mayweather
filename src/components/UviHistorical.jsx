import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";

import { Paper, Typography } from "@material-ui/core";
import {
  Chart,
  ArgumentAxis,
  ValueAxis,
  SplineSeries
} from "@devexpress/dx-react-chart-material-ui";

function UviHistorical(props) {
  if (_.isEmpty(props)) return <i />;
  const { historical } = props;
  return (
    <div>
      <Typography
        variant="h4"
        align="center"
        style={{ color: "white", margin: 20 }}
      >
        Last 30 days UVI
      </Typography>
      <Paper style={{ opacity: 0.7 }}>
        <Chart data={Object.values(historical)}>
          <ArgumentAxis showGrid showLabels={false} />
          <ValueAxis />
          <SplineSeries valueField="value" argumentField="date_iso" />
        </Chart>
      </Paper>
    </div>
  );
}

UviHistorical.propTypes = {
  historical: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.number.isRequired,
      date_iso: PropTypes.string.isRequired
    }).isRequired
  ).isRequired
};

export default UviHistorical;
