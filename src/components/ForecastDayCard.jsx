import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";

import { Typography, Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import { weatherBackgroundFor, WeatherIcon } from "./WeatherIcon";

const useStyles = makeStyles(() => ({
  root: {
    position: "relative",
    padding: "12px 24px",
    backgroundSize: "cover",
    backgroundPosition: "bottom",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    backgroundBlendMode: "color",
    maxWidth: "10rem",
    height: "10rem",
    margin: "0 auto",
    color: "white",
    borderRadius: "1rem 3rem",
    display: "flex",
    justifyContent: "center",
    cursor: "pointer",
    bottom: "0rem",
    transition:
      "bottom 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, border-radius 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
    "&:hover": {
      bottom: "2rem",
      borderRadius: "1rem"
    },
    "&:focus": {
      borderRadius: "3rem 1rem",
      bottom: "1rem"
    }
  },
  mainIcon: {
    display: "flex",
    flexFlow: "column wrap",
    width: "100%",
    alignContent: "center",
    "& div": {
      fontSize: "3rem",
      flexGrow: 50,
      alignSelf: "center",
      alignContent: "baseline"
    },
    "& h4": {
      flexGrow: 20,
      textAlign: "center",
      lineHeight: "1rem",
      fontSize: "1rem",
      fontWeight: "bold"
    },
    "& h6": {
      flexGrow: 5,
      textAlign: "center",
      fontSize: "1rem",
      fontWeight: "normal"
    }
  },
  minmaxRow: {
    display: "flex",
    alignContent: "center",
    flexFlow: "row wrap",
    width: "100%",
    alignItems: "center",
    justifyContent: "space-between",
    flexGrow: "0",
    "& div": {
      fontSize: "1rem",
      flexGrow: 0
    },
    "& p": {
      fontSize: "0.75rem",
      flexGrow: 0
    }
  }
}));

function ForecastDayCard(props) {
  if (_.isEmpty(props)) return <i />;

  const { id, dt, temp_min, temp_max } = props;

  const classes = useStyles();
  const days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ];

  const day = date => days[new Date(date * 1000).getDay()];

  const date = d => {
    const dtime = new Date(d * 1000);
    let dayMonth = [dtime.getDate() + 1, dtime.getMonth() + 1];
    dayMonth = dayMonth.map(n => (n < 10 ? `0${n}` : n)); // Zero padding
    return `${dayMonth[0]}/${dayMonth[1]}`;
  };

  return (
    <div>
      <Paper
        tabIndex="0"
        className={classes.root}
        style={{
          backgroundImage: `url("/img/cards/${weatherBackgroundFor(id)}.jpg")`
        }}
      >
        <div className={classes.mainIcon}>
          <WeatherIcon icon={id} />
          <Typography variant="h4">{date(dt)}</Typography>
          <Typography variant="h6">{day(dt)}</Typography>
          <div className={classes.minmaxRow}>
            <WeatherIcon icon="thermometer-exterior" />
            <Typography>{`${Math.round(temp_min)}˚C`}</Typography>
            <WeatherIcon icon="thermometer" />
            <Typography>{`${Math.round(temp_max)}˚C`}</Typography>
          </div>
        </div>
      </Paper>
    </div>
  );
}

ForecastDayCard.propTypes = {
  id: PropTypes.number.isRequired,
  dt: PropTypes.number.isRequired,
  temp_min: PropTypes.number.isRequired,
  temp_max: PropTypes.number.isRequired
};

export default ForecastDayCard;
