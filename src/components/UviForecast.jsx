import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";

import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import UviForecastCard from "./UviForecastCard";

const useStyles = makeStyles(() => ({
  grid: {
    width: "calc(100% + 12rem)",
    marginLeft: "-6rem"
  }
}));

function UviForecast(props) {
  const { forecast } = props;
  if (_.isEmpty(forecast)) return <i />;
  const classes = useStyles();
  return (
    <Grid container justify="center" spacing={2} className={classes.grid}>
      {forecast.map(day => (
        <Grid item xs={2} key={day.date}>
          <UviForecastCard {...day} />
        </Grid>
      ))}
    </Grid>
  );
}

UviForecast.propTypes = {
  forecast: PropTypes.arrayOf(
    PropTypes.shape({
      date: PropTypes.number.isRequired,
      value: PropTypes.number.isRequired
    }).isRequired
  ).isRequired
};

export default UviForecast;
