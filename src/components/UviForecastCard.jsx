import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";

import { Typography, Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  root: {
    position: "relative",
    padding: "12px 24px",
    backgroundSize: "cover",
    backgroundPosition: "bottom",
    background: "grey",
    maxWidth: "10rem",
    height: "10rem",
    margin: "0 auto",
    color: "white",
    borderRadius: "1rem 3rem",
    display: "flex",
    justifyContent: "center",
    cursor: "pointer",
    bottom: "0rem",
    transition:
      "bottom 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms, border-radius 200ms cubic-bezier(0.4, 0, 0.2, 1) 0ms"
  },
  mainIcon: {
    display: "flex",
    flexFlow: "column wrap",
    width: "100%",
    alignContent: "center",
    "& h4": {
      textAlign: "center",
      lineHeight: "1rem",
      fontSize: "1rem",
      fontWeight: "bold"
    },
    "& h6": {
      textAlign: "center",
      fontSize: "1rem",
      fontWeight: "normal"
    }
  }
}));

const gradient = {
  green: {
    background:
      "linear-gradient(120deg, rgba(143,255,172,1) -50%, rgba(0,255,0,1) 21%, rgba(239,255,0,1) 150%)",
    color: "black"
  },
  yellow: {
    background:
      "linear-gradient(120deg, rgba(196,255,0,1) -50%, rgba(255,255,0,1) 50%, rgba(255,190,0,1) 150%)",
    color: "black"
  },
  orange: {
    background:
      "linear-gradient(120deg, rgb(239, 255, 0) -100%, rgb(250, 148, 0) 29%, rgb(251, 120, 0) 69%, rgb(255, 0, 0) 150%)",
    color: "black"
  },
  red: {
    background:
      "linear-gradient(120deg, rgba(255,151,0,1) -50%, rgba(255,0,0,1) 48%, rgba(181,0,255,1) 150%)",
    color: "white"
  },
  violet: {
    background:
      "linear-gradient(120deg, rgb(255, 0, 46) -100%, rgb(141, 1, 213) 20%, rgb(141, 1, 213) 80%, rgb(107, 2, 153) 150%)",
    color: "white"
  }
};

const selecBackground = value => {
  if (value > 11) return gradient.violet;
  if (value > 8) return gradient.red;
  if (value > 6) return gradient.orange;
  if (value > 3) return gradient.yellow;
  return gradient.green;
};

function UviForecastCard(props) {
  const days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday"
  ];
  const day = date => days[new Date(date * 1000).getDay()];
  const dateFun = d => {
    const dt = new Date(d * 1000);
    let dayMonth = [dt.getDate() + 1, dt.getMonth() + 1];
    dayMonth = dayMonth.map(n => (n < 10 ? `0${n}` : n)); // Zero padding
    return `${dayMonth[0]}/${dayMonth[1]}`;
  };
  const classes = useStyles();

  if (_.isEmpty(props)) return <i />;
  const { date, value } = props;
  return (
    <div>
      <Paper
        tabIndex="0"
        className={classes.root}
        style={selecBackground(value)}
      >
        <div className={classes.mainIcon}>
          <Typography variant="h6">{day(date)}</Typography>
          <Typography variant="h4" style={{ flexGrow: 10 }}>
            {dateFun(date)}
          </Typography>
          <Typography variant="h4">UVI</Typography>
          <Typography variant="h3">{value}</Typography>
        </div>
      </Paper>
    </div>
  );
}

UviForecastCard.propTypes = {
  date: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired
};

export default UviForecastCard;
