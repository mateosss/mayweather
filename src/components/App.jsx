import React from "react";
import axios from "axios";
import _ from "lodash";

import { Container, CssBaseline, Typography } from "@material-ui/core";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import Grow from "@material-ui/core/Grow";

import SearchBox from "./SearchBox";
import Forecast from "./Forecast";
import DayCard from "./DayCard";

import Uvi from "./Uvi";

const api = axios.create({
  baseURL: "http://api.openweathermap.org/data/2.5/",
  timeout: 10000
});

const theme = createMuiTheme({
  typography: {
    fontFamily: "Google Sans",
    fontSize: 16,
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500
  }
});

const bodyBackgrounds = [
  "linear-gradient(to top, #3f51b1 0%, #5a55ae 13%, #7b5fac 25%, #8f6aae 38%, #a86aa4 50%, #cc6b8e 62%, #f18271 75%, #f3a469 87%, #f7c978 100%)",
  "linear-gradient(to top, #dbdcd7 0%, #dddcd7 24%, #e2c9cc 30%, #e7627d 46%, #b8235a 59%, #801357 71%, #3d1635 84%, #1c1a27 100%)",
  "linear-gradient(to top, #fcc5e4 0%, #fda34b 15%, #ff7882 35%, #c8699e 52%, #7046aa 71%, #0c1db8 87%, #020f75 100%)",
  "linear-gradient(to right, #b8cbb8 0%, #b8cbb8 0%, #b465da 0%, #cf6cc9 33%, #ee609c 66%, #ee609c 100%)",
  "linear-gradient(to right, #30cfd0 0%, #330867 100%)",
  "linear-gradient(to right, #434343 0%, black 100%)",
  "linear-gradient(to right, #00dbde 0%, #fc00ff 100%)",
  "linear-gradient(-225deg, #22E1FF 0%, #1D8FE1 48%, #625EB1 100%)",
  "linear-gradient(-225deg, #FF057C 0%, #8D0B93 50%, #321575 100%)",
  "linear-gradient(-225deg, #69EACB 0%, #EACCF8 48%, #6654F1 100%)",
  "linear-gradient(-225deg, #231557 0%, #44107A 29%, #FF1361 67%, #FFF800 100%)"
];

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      weather: null,
      forecast: null,
      uvi: null,
      loading: false,
      city: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async getWeather() {
    const { city } = this.state;
    const appid = "3729c16bb3eb585bf69f7e1dbe2ba638";
    const units = "metric";
    try {
      // Load weather and forecast
      this.setState({ loading: true });

      // TODO: Make parallel gets
      const weatherData = await api.get(
        `/weather?q=${city}&APPID=${appid}&units=${units}`
      );
      const forecastData = await api.get(
        `/forecast?q=${city}&APPID=${appid}&units=${units}`
      );

      this.setState({
        weather: weatherData.data,
        forecast: forecastData.data
      });

      // Update background color
      document.body.style.backgroundImage = _.sample(bodyBackgrounds);

      // Load UVI

      const {
        weather: {
          coord: { lat, lon },
          dt: date
        }
      } = this.state;

      const date30 = date - 2592000;
      // TODO: Make parallel gets
      const uviDate = await api.get(
        `/uvi?appid=${appid}&lat=${lat}&lon=${lon}`
      );
      const uviForecast = await api.get(
        `/uvi/forecast?appid=${appid}&lat=${lat}&lon=${lon}&cnt=5`
      );
      const uviHistory = await api.get(
        `/uvi/history?appid=${appid}&lat=${lat}&lon=${lon}&cnt=30&start=${date30}&end=${date}`
      );

      this.setState({
        uvi: {
          current: uviDate.data,
          forecast: uviForecast.data,
          historical: uviHistory.data
        }
      });
    } catch (error) {
      // TODO: Proper error message
      alert(`Error! ${error}`);
    } finally {
      this.setState({ loading: false });
    }
  }

  // TODO: Should we use Uncontrolled Components?
  // https://reactjs.org/docs/uncontrolled-components.html
  handleChange(event) {
    this.setState({ city: event.target.value });
  }

  handleSubmit() {
    this.getWeather();
  }

  render() {
    const { loading, weather, forecast, uvi } = this.state;
    const title = weather
      ? `${weather.name}, ${weather.sys.country}`
      : "May Weather";

    return (
      <MuiThemeProvider theme={theme}>
        <Container maxWidth="md">
          <CssBaseline />
          <Typography
            variant="h2"
            style={{
              color: "white",
              textAlign: "center",
              paddingTop: "3rem",
              fontWeight: "bold"
            }}
          >
            {title}
          </Typography>

          <SearchBox
            loading={loading}
            onChange={this.handleChange}
            onSubmit={this.handleSubmit}
          />

          {weather && (
            <Grow in={!loading}>
              <DayCard weather={weather} />
            </Grow>
          )}

          {forecast && <Forecast {...forecast} />}

          {uvi && <Uvi {...uvi} />}
        </Container>
      </MuiThemeProvider>
    );
  }
}

export default App;
