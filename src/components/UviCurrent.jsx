import React from "react";
import PropTypes from "prop-types";

import { LinearProgress, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  root: {
    height: 50,
    maxWidth: "50rem",
    marginLeft: "auto",
    marginRight: "auto",
    background:
      "linear-gradient(90deg, limegreen, yellow 21%, orange 36%, red 67%, darkviolet 89%)",
    borderRadius: 5
  },
  bar: {
    borderRadius: 0,
    background: "rgba(0,0,0,0.1)",
    borderRight: "4px solid rgba(0,0,0,0.5)"
  },
  tittle: {
    margin: 30,
    fontSize: "3rem",
    color: "white",
    fontWeight: "bold"
  }
});

function UviCurrent(props) {
  const classes = useStyles();
  const { value } = props;

  if (!value) return <i />;
  return (
    <div>
      <Typography
        variant="h4"
        component="h1"
        align="center"
        className={classes.tittle}
      >
        {`UVI: ${value}`}
      </Typography>
      <LinearProgress
        value={value > 14 ? 99.5 : (value * 100) / 14}
        variant="determinate"
        classes={{ root: classes.root, bar: classes.bar }}
      />
    </div>
  );
}

UviCurrent.propTypes = {
  value: PropTypes.number.isRequired
};

export default UviCurrent;
