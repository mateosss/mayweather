import React from "react";
import PropTypes from "prop-types";
import _ from "lodash";

import { Grid } from "@material-ui/core";

import UviCurrent from "./UviCurrent";
import UviForecast from "./UviForecast";
import UviHistorical from "./UviHistorical";

function Uvi(props) {
  if (_.isEmpty(props)) return <i />;
  const { current, forecast, historical } = props;
  return (
    <Grid container direction="column" spacing={4}>
      <Grid item xs>
        <UviCurrent {...current} />
      </Grid>
      <Grid item xs>
        <UviForecast forecast={forecast} />
      </Grid>
      <Grid item xs>
        <UviHistorical historical={historical} />
      </Grid>
    </Grid>
  );
}

Uvi.propTypes = {
  current: PropTypes.shape({
    value: PropTypes.number.isRequired
  }).isRequired,

  forecast: PropTypes.arrayOf(
    PropTypes.shape({
      date: PropTypes.number.isRequired,
      value: PropTypes.number.isRequired
    }).isRequired
  ).isRequired,

  historical: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.number.isRequired,
      date_iso: PropTypes.string.isRequired
    }).isRequired
  ).isRequired
};

export default Uvi;
